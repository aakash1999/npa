<html>
<head>
<title>Registration Form</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="w3.css">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

<script>
	function checkVal(str){
		if (str == "") {
        document.getElementById("txtHint").innerHTML = "";
        return;
    } else { 
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("txtHint").innerHTML = this.responseText;
            }
        };
        xmlhttp.open("GET","checkEmail.php?q="+str,true);
        xmlhttp.send();
    }
	}
	
	function validate(){
		if(document.getElementById("password1").value==document.getElementById("password2").value){
			if(document.getElementById("txtHint").innerHTML == "This email can register"){
				document.getElementById("myForm").action = "sendMail.php";
			}else{
				alert("Enter a valid Email");
			}
		}else{
			alert("Passwords do not match");
		}
	}
</script>
</head>
<body background = "background.jpg">

<div class="w3-container">
  <div class="w3-modal w3-show w3-card-4">
    <div class="w3-modal-content w3-card-4 w3-animate-zoom" style="max-width:600px">
  
      <div class="w3-center w3-container"><h1>Register an Account</h1><br>
      </div>

      <form class="w3-container" method = "post" onsubmit = "validate()" id = "myForm">
        <div class="w3-section">
		  <label><b>Email</b></label>
          <input class="w3-input w3-border w3-margin-bottom" type="email" placeholder="Enter your Email" name="inputEmail" required onkeyup = "checkVal(this.value)">
		  <input type="hidden"><font color="red"><div id="txtHint"></div></font>
          <label><b>Username</b></label>
          <input class="w3-input w3-border w3-margin-bottom" type="text" placeholder="Enter Username" name="usrname" required>
          <label><b>Password</b></label>
          <input class="w3-input w3-border" type="text" placeholder="Enter Password" name="psw" required id="password1"><br>
		  <label><b>Password Again</b></label>
          <input class="w3-input w3-border" type="text" placeholder="Enter Password Again" name="repeatpsw" required id = "password2">
          
		  <button class="w3-button w3-block w3-green w3-section w3-padding" type="submit">Register</button>
        </div>
      </form>

      <div class="w3-container w3-border-top w3-padding-16 w3-light-grey">
        <span class="w3-right w3-padding w3-hide-small"><a href="#" style = "text-decoration:none;" class = "w3-button">Have an account?Log in</a></span>
      </div>

    </div>
  </div>
</div>
            
</body>
</html>
