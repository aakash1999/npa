<?php
	 session_start();
	 
		$host = 'localhost';
        $user = 'root';
        $pass = '';
		$dbname = "npa";
		
		$conn = mysqli_connect($host, $user, $pass,$dbname);
		
		if($_SERVER['REQUEST_METHOD'] == "POST"){
			$oldPassword = $_POST['oldpassword'];
			$newPassword = $_POST['newpassword'];
			$email = $_POST['email'];
			
			$sql = "SELECT * FROM users WHERE email = '$email' AND password = '$oldPassword'";
			
			$result = mysqli_query($conn,$sql);
			$count = mysqli_num_rows($result);
			
			if($count == 1){
				$sql1 = "UPDATE users SET password = '$newPassword' WHERE email = '$email'";
				mysqli_query($conn,$sql1);
				header("Location: ChangeSuccess.php");
				
			}
		}

		mysqli_close($conn);

 ?>




<html>
<title>Change password</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="w3.css">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<body background = "background.jpg">

<div class="w3-container">
  <div class="w3-modal w3-show w3-card-4">
    <div class="w3-modal-content w3-card-4 w3-animate-zoom" style="max-width:600px">
  
      <div class="w3-center w3-container"><h1>Change Password</h1><br>
      </div>

      <form class="w3-container" action="<?php echo $_SERVER["PHP_SELF"];?>" method = "post">
        <div class="w3-section">
		<label><b>Email</b></label>
          <input class="w3-input w3-border w3-margin-bottom" type="email" placeholder="Enter your registered email" name="email" required>
          <label><b>Old Password</b></label>
          <input class="w3-input w3-border w3-margin-bottom" type="text" placeholder="Enter your old password" name="oldpassword" required>
          <label><b>New Password</b></label>
          <input class="w3-input w3-border" type="text" placeholder="Enter new password" name="newpassword" required>
          <button class="w3-button w3-block w3-green w3-section w3-padding" type="submit">Change Password</button>
        </div>
      </form>

      <div class="w3-container w3-border-top w3-padding-16 w3-light-grey">
        <span class="w3-right w3-padding w3-hide-small"><a href="forgotPassword.php" style = "text-decoration:none;">Forgot password?</a></span>
      </div>

    </div>
  </div>
</div>
            
</body>
</html>
