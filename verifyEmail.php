<html>
<head>
<title>
Verify Email
</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="w3.css">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>
<body background = "background.jpg">

<div class="w3-container">
  <div class="w3-modal w3-show w3-card-4">
    <div class="w3-modal-content w3-card-4 w3-animate-zoom" style="max-width:600px">
  
      <div class="w3-center w3-container"><h1>Verify your Email</h1><br>
      </div>

      <form class="w3-container" action="registerFinal.php" method = "post">
        <div class="w3-section">
		  <label><b>Verification Code</b></label>
          <input class="w3-input w3-border w3-margin-bottom" type="text" placeholder="Enter Verification code" name="code" required>
			
          
		  <button class="w3-button w3-block w3-green w3-section w3-padding" type="submit">Register</button>
        </div>
      </form>

    </div>
  </div>
</div>
            
</body>
</html>