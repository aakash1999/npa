
<html>
<title>Login Form</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="w3.css">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<body background = "background.jpg">

<div class="w3-container">
  <div class="w3-modal w3-show w3-card-4">
    <div class="w3-modal-content w3-card-4 w3-animate-zoom" style="max-width:600px">
  
      <div class="w3-center w3-container"><h1>Login</h1><br>
      </div>

      <form class="w3-container" action="dblogin.php" method = "post">
        <div class="w3-section">
          <label><b>Email</b></label>
          <input class="w3-input w3-border w3-margin-bottom" type="email" placeholder="Enter your registered email" name="usrname" required>
          <label><b>Password</b></label>
          <input class="w3-input w3-border" type="text" placeholder="Enter Password" name="psw" required>
          <button class="w3-button w3-block w3-green w3-section w3-padding" type="submit">Login</button>
        </div>
      </form>

      <div class="w3-container w3-border-top w3-padding-16 w3-light-grey">
        <span class="w3-right w3-padding w3-hide-small"><a href="forgotPassword.php" style = "text-decoration:none;">Forgot password?</a></span>
		<span class="w3-right w3-padding w3-hide-small"><a href="changePassword.php" style = "text-decoration:none;">Change password</a></span>
      </div>

    </div>
  </div>
</div>
            
</body>
</html>
